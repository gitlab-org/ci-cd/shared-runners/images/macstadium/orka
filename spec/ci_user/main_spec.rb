# frozen_string_literal: true

require 'spec_helper'

context 'roles/ci_user' do
  # shell should be zsh
  describe user('gitlab') do
    it { should have_login_shell '/bin/zsh' }
  end

  # .zshenv should exist
  describe file('/Users/gitlab/.zshenv') do
    it { should be_file }
  end

  # .zshrc should exist
  describe file('/Users/gitlab/.zshrc') do
    it { should be_file }
  end

  # check that the env is setup correctly
  describe command('printenv') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/^LANG=en_US.UTF-8$/) }
    its(:stdout) { should match(/^LC_ALL=en_US.UTF-8$/) }
    its(:stdout) { should match(%r{^PATH=(.*:)?/usr/local/bin(:.*)?$}) }
  end

  # this should not ask for a password
  describe command('sudo whoami') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match('root') }
  end
end
