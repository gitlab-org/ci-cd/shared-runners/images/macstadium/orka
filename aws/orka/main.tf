terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.32"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 3.1.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.3"
    }
  }

  backend "http" {
    # Project # 19922160 = https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka
    address        = "https://gitlab.com/api/v4/projects/19922160/terraform/state/orka"
    lock_address   = "https://gitlab.com/api/v4/projects/19922160/terraform/state/orka/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/19922160/terraform/state/orka/lock"
    username       = "glpat"
    # password = "" # should be provided via `-backend-config="password=$GITLAB_PAT"` during `terraform init`
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

  required_version = ">= 1.3.1"
}

# 8888888b.  8888888b.   .d88888b.  888     888 8888888 8888888b.  8888888888 8888888b.   .d8888b.
# 888   Y88b 888   Y88b d88P" "Y88b 888     888   888   888  "Y88b 888        888   Y88b d88P  Y88b
# 888    888 888    888 888     888 888     888   888   888    888 888        888    888 Y88b.
# 888   d88P 888   d88P 888     888 Y88b   d88P   888   888    888 8888888    888   d88P  "Y888b.
# 8888888P"  8888888P"  888     888  Y88b d88P    888   888    888 888        8888888P"      "Y88b.
# 888        888 T88b   888     888   Y88o88P     888   888    888 888        888 T88b         "888
# 888        888  T88b  Y88b. .d88P    Y888P      888   888  .d88P 888        888  T88b  Y88b  d88P
# 888        888   T88b  "Y88888P"      Y8P     8888888 8888888P"  8888888888 888   T88b  "Y8888P"

provider "aws" {
  region = "eu-west-1"

  # make sure we don't run this on a personal account
  allowed_account_ids = [
    "915502504722" # shared runner-saas gitlab sandbox account
  ]
}

# 8888888b.        d8888 88888888888     d8888
# 888  "Y88b      d88888     888        d88888
# 888    888     d88P888     888       d88P888
# 888    888    d88P 888     888      d88P 888
# 888    888   d88P  888     888     d88P  888
# 888    888  d88P   888     888    d88P   888
# 888  .d88P d8888888888     888   d8888888888
# 8888888P" d88P     888     888  d88P     888

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state         = "available"
  exclude_names = ["eu-west-1c"] # mac2.metal not available there
}

data "aws_caller_identity" "current" {}

data "aws_ami" "macos_runner" {
  most_recent = true

  filter {
    name   = "name"
    values = ["macos-12-monterey-arm64-runner-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["arm64_mac"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["915502504722"] # shared runner-saas gitlab sandbox account
}

# 8888888b.  8888888888 .d8888b.   .d88888b.  888     888 8888888b.   .d8888b.  8888888888 .d8888b.
# 888   Y88b 888       d88P  Y88b d88P" "Y88b 888     888 888   Y88b d88P  Y88b 888       d88P  Y88b
# 888    888 888       Y88b.      888     888 888     888 888    888 888    888 888       Y88b.
# 888   d88P 8888888    "Y888b.   888     888 888     888 888   d88P 888        8888888    "Y888b.
# 8888888P"  888           "Y88b. 888     888 888     888 8888888P"  888        888           "Y88b.
# 888 T88b   888             "888 888     888 888     888 888 T88b   888    888 888             "888
# 888  T88b  888       Y88b  d88P Y88b. .d88P Y88b. .d88P 888  T88b  Y88b  d88P 888       Y88b  d88P
# 888   T88b 8888888888 "Y8888P"   "Y88888P"   "Y88888P"  888   T88b  "Y8888P"  8888888888 "Y8888P"

resource "aws_licensemanager_license_configuration" "license_config" {
  name                  = "macos-runners"
  description           = "Pass through configuration for macOS Host Resource Group"
  license_counting_type = "Core"
}

resource "aws_cloudformation_stack" "host_resource_group" {
  name = "macos-runners-resource-group"

  template_body = <<STACK
{
    "Resources" : {
        "HostResourceGroup": {
            "Type": "AWS::ResourceGroups::Group",
            "Properties": {
                "Name": "macos-runners-resource-group",
                "Configuration": [
                    {
                        "Type": "AWS::EC2::HostManagement",
                        "Parameters": [
                            {
                                "Name": "allowed-host-based-license-configurations",
                                "Values": [
                                    "${aws_licensemanager_license_configuration.license_config.arn}"
                                ]
                            },
                            {
                                "Name": "allowed-host-families",
                                "Values": [
                                    "mac2"
                                ]
                            },
                            {
                                "Name": "auto-allocate-host",
                                "Values": [
                                    "true"
                                ]
                            },
                            {
                                "Name": "auto-release-host",
                                "Values": [
                                    "true"
                                ]
                            }
                        ]
                    },
                    {
                        "Type": "AWS::ResourceGroups::Generic",
                        "Parameters": [
                            {
                                "Name": "allowed-resource-types",
                                "Values": [
                                    "AWS::EC2::Host"
                                ]
                            },
                            {
                                "Name": "deletion-protection",
                                "Values": [
                                    "UNLESS_EMPTY"
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    },
    "Outputs" : {
        "ResourceGroupArn" : {
            "Description": "ResourceGroup Arn",
            "Value" : { "Fn::GetAtt" : ["HostResourceGroup", "Arn"] },
            "Export" : {
              "Name": "macos-runners-resource-group"
            }
        }
    }
}
STACK
}

locals {
  ecr_policy = jsonencode({
    rules = [
      {
        rulePriority = 1
        description  = "Keep last 60 images built on the main branch (no mr- prefix). Note each pipeline generates multiple p- images, so this keeps less than 60 *pipelines* in total"
        selection = {
          tagStatus     = "tagged"
          tagPrefixList = ["p-"]
          countType     = "imageCountMoreThan"
          countNumber   = 60
        }
        action = {
          type = "expire"
        }
      },
      {
        rulePriority = 2
        description  = "Keep images built from Merge Requests for 30 days"
        selection = {
          tagStatus     = "tagged"
          tagPrefixList = ["mr-"]
          countType     = "sinceImagePushed"
          countUnit     = "days"
          countNumber   = 30
        }
        action = {
          type = "expire"
        }
      },
      {
        rulePriority = 3
        description  = "Expire untagged images after 14 days"
        selection = {
          tagStatus   = "untagged"
          countType   = "sinceImagePushed"
          countUnit   = "days"
          countNumber = 14
        }
        action = {
          type = "expire"
        }
      }
    ]
  })
}

resource "aws_ecr_repository" "macos_base" {
  name                 = "macos/base"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "macos_base" {
  repository = aws_ecr_repository.macos_base.name
  policy     = local.ecr_policy
}

resource "aws_ecr_repository" "macos_toolchain" {
  name                 = "macos/toolchain"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "macos_toolchain" {
  repository = aws_ecr_repository.macos_toolchain.name
  policy     = local.ecr_policy
}

resource "aws_ecr_repository" "macos_xcode" {
  name                 = "macos/xcode"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "macos_xcode" {
  repository = aws_ecr_repository.macos_xcode.name
  policy     = local.ecr_policy
}

resource "aws_ecr_repository" "macos_runner" {
  name                 = "macos/runner"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "macos_runner" {
  repository = aws_ecr_repository.macos_runner.name
  policy     = local.ecr_policy
}

resource "aws_ecr_repository" "macos_release" {
  name                 = "macos/release"
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "macos_release" {
  repository = aws_ecr_repository.macos_release.name
  policy     = local.ecr_policy
}

resource "aws_iam_instance_profile" "macos_runner" {
  name_prefix = "macos-internal-runner-"
  role        = aws_iam_role.macos_runner.name
}

resource "aws_iam_role" "macos_runner" {
  name_prefix = "macos-internal-runner-"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "ssm_managed_instance_core" {
  role       = aws_iam_role.macos_runner.id
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_role_policy" "allow_lifecycle_completion" {
  name_prefix = "allow-lifecycle-completion-"
  role        = aws_iam_role.macos_runner.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["autoscaling:CompleteLifecycleAction"]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role_policy" "allow-ecr-push" {
  name_prefix = "allow-ecr-push-"
  role        = aws_iam_role.macos_runner.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:UploadLayerPart",
          "ecr:InitiateLayerUpload",
          "ecr:BatchCheckLayerAvailability",
          "ecr:PutImage",
          "ecr:GetAuthorizationToken"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_launch_template" "macos_runner" {
  name_prefix            = "macos-internal-runner-"
  update_default_version = true

  instance_type                        = "mac2.metal"
  image_id                             = data.aws_ami.macos_runner.id
  instance_initiated_shutdown_behavior = "terminate"
  ebs_optimized                        = true

  user_data = filebase64("${path.module}/userdata.sh")

  disable_api_stop        = true
  disable_api_termination = true

  network_interfaces {
    associate_public_ip_address = true
  }

  iam_instance_profile {
    arn = aws_iam_instance_profile.macos_runner.arn
  }

  license_specification {
    license_configuration_arn = aws_licensemanager_license_configuration.license_config.arn
  }

  placement {
    tenancy                 = "host"
    host_resource_group_arn = aws_cloudformation_stack.host_resource_group.outputs["ResourceGroupArn"]
  }

  metadata_options {
    http_endpoint          = "enabled"
    instance_metadata_tags = "enabled" # instance needs access to its tag to get the ASG name for the lifecycle hook completion
  }

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "macos-internal-runner"
    }
  }

  tag_specifications {
    resource_type = "volume"

    tags = {
      Name = "macos-internal-runner"
    }
  }

  tag_specifications {
    resource_type = "network-interface"

    tags = {
      Name = "macos-internal-runner"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "macos_runner" {
  name_prefix = "macos-internal-runner-"

  # Desired Capacity
  desired_capacity = 2
  max_size         = 3
  min_size         = 1

  availability_zones = data.aws_availability_zones.available.names

  # don't wait for capacity when creating the ASG, it will be very long
  wait_for_capacity_timeout = 0

  # don't keep instances for more than a week
  # these are shell runners and they could become corrupted or
  # iterations could leave behind some state that the CI relies on that we don't know about
  max_instance_lifetime = 7 * 24 * 3600

  timeouts {
    delete = "2h10m" # give time for runner to gracefully shutdown
  }

  launch_template {
    id      = aws_launch_template.macos_runner.id
    version = "$Latest"
  }

  # macOS instances take a long time to be provisioned, more than 40min in most cases
  # We don't want the ASG to execute any health checks before the instance is actually available
  # So we wait for the user data script to terminate by using a lifecycle hook.
  initial_lifecycle_hook {
    name                 = "startup"
    default_result       = "ABANDON"
    heartbeat_timeout    = 60 * 60 # mac2.metal instances can take up to 40min to be provisioned. Add a bit of margin.
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"
  }

  # We want to wait for jobs to finish before terminating instances.
  # A process running on the maching will watch for this lifecycle change and gracefully shutdown the runner
  initial_lifecycle_hook {
    name                 = "graceful-shutdown"
    default_result       = "ABANDON"
    heartbeat_timeout    = 7200 # this is the max value, we want to give as much time as possible to jobs
    lifecycle_transition = "autoscaling:EC2_INSTANCE_TERMINATING"
  }

  lifecycle {
    create_before_destroy = true
  }
}

#  .d88888b.  888     888 88888888888 8888888b.  888     888 88888888888 .d8888b.
# d88P" "Y88b 888     888     888     888   Y88b 888     888     888    d88P  Y88b
# 888     888 888     888     888     888    888 888     888     888    Y88b.
# 888     888 888     888     888     888   d88P 888     888     888     "Y888b.
# 888     888 888     888     888     8888888P"  888     888     888        "Y88b.
# 888     888 888     888     888     888        888     888     888          "888
# Y88b. .d88P Y88b. .d88P     888     888        Y88b. .d88P     888    Y88b  d88P
#  "Y88888P"   "Y88888P"      888     888         "Y88888P"      888     "Y8888P"

output "region" {
  value = data.aws_region.current.name
}

output "host_resource_group_arn" {
  value = aws_cloudformation_stack.host_resource_group.outputs["ResourceGroupArn"]
}
output "license_configuration_arn" {
  value = aws_licensemanager_license_configuration.license_config.arn
}
