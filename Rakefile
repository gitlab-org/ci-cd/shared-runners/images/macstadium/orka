# frozen_string_literal: true

require 'rake'
require 'rspec/core/rake_task'

task default: :spec

targets = {
  toolchain: %i[ci_user os_prepare spotlight package_managers],
  xcode: [:spotlight],
  runner: [:spotlight],
  brew: [],
  release: []
}
targets[:all] = targets.values.flatten

RSpec::Core::RakeTask.new(:spec) do |t|
  roles = ENV.fetch('ROLES_TO_TEST', '').split(',').reject(&:empty?)
  specs = roles.map{|role| targets[role.to_sym]}.flatten.uniq
  t.pattern = "spec/{#{specs.join(',')}}/*_spec.rb"
end
