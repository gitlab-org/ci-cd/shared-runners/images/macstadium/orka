#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

ORKA_WRAPPER=${ORKA_WRAPPER:-orka}
ORKA_DEV_ENV=${ORKA_DEV_ENV:-}

VM_NAME="$(whoami | sed 's/\./-/')"
XCODE_VERSION="12"
BASE_IMAGE="macos-10.15-90G-ssh.img"
ROLE="site"
TOOLCHAIN_VERSION="catalina"
COPY_SSH_ID="true"

HELP="Usage: scripts/dev-vm **kwargs [status|new|destroy|provision|ssh]
  -i|--base-image         OS image to use (default: $BASE_IMAGE)
  -n|--vm-name            Name of the Orka VM to create (default: $VM_NAME)
  -r|--role               Ansible YAML file to run, or spec suite to execute (default: $ROLE)
  -x|--xcode-version      Major Xcode version (default: $XCODE_VERSION)
  -t|--toolchain-version  Major Xcode version (default: $TOOLCHAIN_VERSION)
  -s|--copy-ssh-id        Copy public SSH key to machine (default: $COPY_SSH_ID)
"

while [[ "$#" -gt 1 ]]; do
    case $1 in
        -i|--base-image) BASE_IMAGE="$2"; shift ;;
        -n|--vm-name) VM_NAME="$2"; shift ;;
        -r|--role) ROLE="$2"; shift ;;
        -x|--xcode-version) XCODE_VERSION="$2"; shift ;;
        -t|--toolchain-version) TOOLCHAIN_VERSION="$2"; shift ;;
        -s|--copy-ssh-id) COPY_SSH_ID="$2"; shift ;;
    esac
    shift
done

_orka() {
  "${ORKA_WRAPPER}" ${ORKA_DEV_ENV} $@
}

if [[ "${ORKA_WRAPPER}" == "orka" ]]; then
  if ! command -v "${ORKA_WRAPPER}" &> /dev/null; then echo 'orka not found. Install with "brew install orka"'; exit 1; fi
fi
if ! command -v jq &> /dev/null; then echo 'jq not found. Install with "brew install jq"'; exit 1; fi
if ! command -v socat &> /dev/null; then echo 'socat not found. Install with "brew install socat"'; exit 1; fi
if ! command -v sshpass &> /dev/null; then echo 'sshpass not found. Install with "brew install gitlab/shared-runners/sshpass"'; exit 1; fi

function addr() {
  VM_NAME=$1
  local -n _IP=$2
  local -n _PORT=$3
  ADDR=$(_orka vm list --json | jq -r ".virtual_machine_resources[] | select(.virtual_machine_name == \"$VM_NAME\") | .status[0] | \"\\(.virtual_machine_ip):\\(.ssh_port)\"")
  _IP=$(echo "$ADDR" | cut -d: -f1)
  _PORT=$(echo "$ADDR" | cut -d: -f2)
}

COMMAND="${1:-new}"
case "$COMMAND" in

  status | state)

    _orka vm status --vm "$VM_NAME" -y

    ;;

  new | create)

    if ! _orka vm list --json | jq -e ".virtual_machine_resources[] | select(.virtual_machine_name == \"$VM_NAME\")" > /dev/null; then
      IO_BOOST="true"
      if [[ "$BASE_IMAGE" == *"10.13"* ]] || [[ "$BASE_IMAGE" == *"10.14"* ]]; then
        IO_BOOST="false"
      fi

      _orka vm create --vm "$VM_NAME" --base-image "$BASE_IMAGE" -c 4 --vcpu 4 --vnc --io-boost "$IO_BOOST" -y

      addr "$VM_NAME" IP PORT

      echo -n "Waiting for machine to boot..."
      # uses socat to connect and wait for the server to say something, which will be the SSH version info:
      while ! socat -T2 -U stdout "tcp:$IP:$PORT,connect-timeout=2,readbytes=3" 2>/dev/null | grep SSH >/dev/null; do
        sleep 1
        echo -n "."
      done
      echo "Connected!"

      if [ "$COPY_SSH_ID" == "true" ]; then
        echo "Copying SSH public key"
        COPY_ID="ssh-copy-id -o Port=$PORT -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@$IP"
        eval "sshpass -p gitlab $COPY_ID" || ( echo "Unable to use default password. Enter manually."; echo; eval "$COPY_ID" )
      fi
    fi

    $0 --vm-name "$VM_NAME" status
    ;;

  destroy | delete | purge | rm)

    if _orka vm list --json | jq -e ".virtual_machine_resources[] | select(.virtual_machine_name == \"$VM_NAME\")" > /dev/null; then
      _orka vm purge --vm "$VM_NAME"
    else
      echo "Error: VM $VM_NAME does not exist."
      exit 1
    fi

    ;;

  commit)

    if _orka vm list --json | jq -e ".virtual_machine_resources[] | select(.virtual_machine_name == \"$VM_NAME\")" > /dev/null; then
      VM_ID=$(_orka vm list --json | jq -r ".virtual_machine_resources[] | select(.virtual_machine_name == \"$VM_NAME\").status[0].virtual_machine_id")
      _orka image commit -v "$VM_ID"
    else
      echo "Error: VM $VM_NAME does not exist."
      exit 1
    fi

    ;;

  ssh)

    addr "$VM_NAME" IP PORT
    ssh -o Port="$PORT" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null "gitlab@$IP"

    ;;

  spec)

    addr "$VM_NAME" IP PORT
    TARGET_HOST=$IP TARGET_PORT=$PORT TOOLCHAIN_VERSION=$TOOLCHAIN_VERSION bundle exec rake "spec:$ROLE"

    ;;

  provision)

    # Generate a key for the service account and store it in the project root directory
    # https://console.cloud.google.com/iam-admin/serviceaccounts/details/116590241271226936536?authuser=1&project=group-verify-df9383
    GCS_KEYFILE=$(cat group-verify-df9383-*.json)
    export GCS_KEYFILE

    # TODO: Skipping setting a new password because neither --ask-become-pass nor --ask-pass set ansible_become_password
    # StrictHostKeyChecking=no is needed for sshpass to work when it is the first time connecting to a host
    pipenv run ansible-playbook \
      --inventory inventory/orka-cli \
      --limit "$VM_NAME" \
      --user gitlab \
      --ask-become-pass \
      --ssh-common-args "-o StrictHostKeyChecking=no" \
      --extra-vars "xcode_version=$XCODE_VERSION" \
      --extra-vars "toolchain_version=$TOOLCHAIN_VERSION" \
      --skip-tags new_password \
      "$ROLE.yml"

    ;;

  *)

    echo -e "$HELP"
    exit 1

    ;;

esac
