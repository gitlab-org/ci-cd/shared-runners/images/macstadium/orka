packer {
  required_version = "~> 1.8.3"

  required_plugins {
    tart = {
      version = ">= 0.5.2"
      source  = "github.com/cirruslabs/tart"
    }
  }
}

variable "source_image" {
  type    = string
  default = "915502504722.dkr.ecr.eu-west-1.amazonaws.com/macos/base:12"
}
variable "vm_name" {
  type    = string
  default = "packer"
}
variable "role" {
  type    = string
  default = "toolchain"
}
variable "toolchain_version" {
  type    = string
  default = "monterey"
}
variable "xcode_version" {
  type    = string
  default = "13"
}

source "tart-cli" "tart" {
  vm_base_name = "${var.source_image}"
  vm_name      = "${var.vm_name}"
  cpu_count    = 4
  memory_gb    = 8
  disk_size_gb = 90
  ssh_password = "gitlab"
  ssh_username = "gitlab"
  ssh_timeout  = "120s"
  headless     = true
  disable_vnc  = true
}

build {
  sources = ["source.tart-cli.tart"]

  // For local use only as it needs interactivity
  // error-cleanup-provisioner "breakpoint" {
  //   note = "If needed, waiting for you to investigate error. Open a tunnel with `sshpass -p gitlab -- ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@$(tart ip --wait 120 ${var.vm_name})`"
  // }

  provisioner "ansible" {
    playbook_file   = "${var.role}.yml"
    user            = "gitlab"
    extra_arguments = ["--extra-vars", "toolchain_version=${var.toolchain_version}", "--extra-vars", "xcode_version=${var.xcode_version}"]
  }
}
